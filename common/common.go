package common

import (
	"github.com/sirupsen/logrus"
	"os"
	"strconv"
	"time"
)

func GetEnvString(key, fallback string) string {
	value, exists := os.LookupEnv(key)
	if !exists {
		value = fallback
	}
	return value
}

func GetEnvUInt(key string, fallback uint) uint {
	var value uint
	valueString, exists := os.LookupEnv(key)
	if exists {
		v64, err := strconv.ParseUint(valueString, 10, 32)
		if err != nil {
			logrus.Fatalf("GetEnvUInt ParseUint fatal: %v", err)
		} else {
			value = uint(v64)
		}
	} else {
		value = fallback
	}
	return value
}

func RangeDate(start, end time.Time) func() time.Time {
	y, m, d := start.Date()
	start = time.Date(y, m, d, 0, 0, 0, 0, time.Local)
	y, m, d = end.Date()
	end = time.Date(y, m, d, 0, 0, 0, 0, time.Local)

	return func() time.Time {
		if start.After(end) {
			return time.Time{}
		}
		date := start
		start = start.AddDate(0, 0, 1)
		return date
	}
}
