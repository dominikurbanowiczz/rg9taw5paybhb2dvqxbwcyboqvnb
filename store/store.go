package store

import (
	"encoding/json"
	"errors"
	"fmt"
	"github.com/sirupsen/logrus"
	"io/ioutil"
	"net/http"
	"sync"
	"time"
	"url-collector/common"
)

var store *Store

type Store struct {
	data     *data
	jobsChan chan *job
}

// data is storing all urls retrieved from nasa api
type data struct {
	dataMap map[string]string
	lock    *sync.RWMutex
}

type job struct {
	date         string
	responseChan chan result
}

type result struct {
	url string
	err error
}

func init() {
	store = &Store{
		data:     &data{dataMap: make(map[string]string), lock: &sync.RWMutex{}},
		jobsChan: make(chan *job, 5),
	}
	workersQuantity := common.GetEnvUInt("CONCURRENT_REQUESTS", 5)
	store.sendWorkers(workersQuantity)
}

func (s *Store) sendWorkers(workersQuantity uint) {
	// keep in mind that more workers means more parallel requests to nasa api
	for w := 1; w <= int(workersQuantity); w++ {
		logrus.Infof("sent worker #%v", w)
		go s.sendWorker()
	}
}

func (s *Store) sendWorker() {
	for job := range s.jobsChan {
		// try to retrieve from memory - maybe other worker just finished same job
		if url, ok := s.data.get(job.date); ok {
			job.responseChan <- result{url: url, err: nil}
			close(job.responseChan)
		} else {
			url, err := fetchUrlFromNasaApi(job.date)
			job.responseChan <- result{url: url, err: err}
			close(job.responseChan)
			s.data.set(job.date, url)
		}
	}
}

func (d *data) get(key string) (url string, ok bool) {
	d.lock.RLock()
	defer d.lock.RUnlock()
	url, ok = d.dataMap[key]
	return
}

func (d *data) set(key, value string) {
	d.lock.Lock()
	defer d.lock.Unlock()
	d.dataMap[key] = value
}

func (s *Store) GetUrls(startDateStr, endDateStr string) ([]string, error) {
	var urls []string

	// validate dates and if they are valid return as time.Time
	startDate, endDate, err := parseDates(startDateStr, endDateStr)
	if err != nil {
		return urls, err
	}
	// iterating from start to end date (including both of them)
	for rd := common.RangeDate(startDate, endDate); ; {
		date := rd()
		if date.IsZero() {
			break
		}
		dateStr := date.Format("2006-01-02")
		url, err := s.GetUrl(dateStr)
		if err != nil {
			return urls, err
		}
		urls = append(urls, url)
	}
	return urls, nil
}

func (s *Store) GetUrl(date string) (url string, err error) {
	// try to retrieve from memory
	url, ok := s.data.get(date)
	if ok {
		return
	} else {
		// send job to worker
		resultsChan := make(chan result, 1)
		s.jobsChan <- &job{date: date, responseChan: resultsChan}
		// receive job result from worker
		for result := range resultsChan {
			url, err = result.url, result.err
		}
		return
	}
}

func GetStore() *Store {
	return store
}

func parseDates(startDateStr, endDateStr string) (startDate, endDate time.Time, err error) {
	startDate, err = time.Parse("2006-01-02", startDateStr)
	if err != nil {
		return
	}
	endDate, err = time.Parse("2006-01-02", endDateStr)
	if err != nil {
		return
	}
	// check if start date is after end date
	if startDate.After(endDate) {
		errorMessage := fmt.Sprintf("parseDates err: start_date=%s, end_date=%s start_date is after end_date", startDateStr, endDateStr)
		err = errors.New(errorMessage)
		return
	}
	// check if start date is greater or equal first nasa post
	nasaApiMinDate := time.Date(1995, 6, 16, 0, 0, 0, 0, time.UTC)
	if startDate.Before(nasaApiMinDate) {
		errorMessage := fmt.Sprintf("parseDates err: start_date=%s, min start_date is 1995-6-16", startDateStr)
		err = errors.New(errorMessage)
		return
	}
	// if end date is from the future set it equal today's date
	today := time.Now().Truncate(24 * time.Hour)
	if endDate.After(today) {
		endDate = today
		return
	}
	return
}

func fetchUrlFromNasaApi(date string) (url string, err error) {
	apiUrl := fmt.Sprintf("https://api.nasa.gov/planetary/apod?api_key=%s&date=%s",
		common.GetEnvString("api_key", "DEMO_KEY"), date)
	response, err := http.Get(apiUrl)
	if err != nil {
		return
	}

	body, err := ioutil.ReadAll(response.Body)
	if err != nil {
		return
	}

	var responseData map[string]string
	err = json.Unmarshal(body, &responseData)

	url, ok := responseData["url"]
	if !ok {
		err = errors.New("fetchUrlFromNasaApi error: can't find key 'url' in response data")
		return
	}
	return
}
