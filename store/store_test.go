package store

import (
	"testing"
	"time"
)

func TestFetchUrlFromNasaApi(t *testing.T) {
	todayStr := time.Now().Format("2006-01-02")
	url, err := fetchUrlFromNasaApi(todayStr)
	if err != nil {
		t.Errorf("TestFetchUrlFromNasaApi err: %v", err)
	}
	t.Log(url)
}

func TestParseDates(t *testing.T) {
	// try correctly formatted dates
	_, _, err := parseDates("2021-01-01", "2021-01-02")
	if err != nil {
		t.Errorf("parseDates returned error %v for proper dates", err)
	}
	// check date before earliest nasa post
	_, _, err = parseDates("1900-01-01", "2021-01-01")
	if err == nil {
		t.Errorf("parseDates didn't detect wrong dates range, date should be between Jun 16, 1995 and today")
	}
	// check start date from the future
	_, _, err = parseDates("2121-01-01", "2021-01-01")
	if err == nil {
		t.Errorf("parseDates didn't detect wrong dates range, date should be between Jun 16, 1995 and today")
	}
	// check start date lower than end date
	_, _, err = parseDates("2021-01-02", "2021-01-01")
	if err == nil {
		t.Errorf("parseDates didn't detect end date lower than start date")
	}
	// check dates in wrong format
	_, _, err = parseDates("01-01-2021", "2021/01/01")
	if err == nil {
		t.Errorf("parseDates didn't detect wrong dates formats")
	}
}

func TestStore_GetUrls(t *testing.T) {
	howManyDays := 3
	endDate := time.Now()
	startDate := endDate.AddDate(0, 0, -howManyDays)

	endDateStr := endDate.Format("2006-01-02")
	startDateStr := startDate.Format("2006-01-02")

	s := GetStore()

	// check retrieving urls
	urls, err := s.GetUrls(startDateStr, endDateStr)
	if err != nil {
		t.Errorf("GetUrls returned error %v", err)
	}

	// check retrieved urls quantity
	expectedUrlsQuantity := howManyDays + 1
	urlsQuantity := len(urls)
	if urlsQuantity != expectedUrlsQuantity {
		t.Errorf("expectedUrlsQuantity is not equal urlsQuantity, %v != %v", expectedUrlsQuantity, urlsQuantity)
	}

	// compare first and last url retrieved from store and nasa api
	firstStoreUrl := urls[0]
	lastStoreUrl := urls[len(urls)-1]
	fistNasaApiUrl, _ := fetchUrlFromNasaApi(startDateStr)
	lastNasaApiUrl, _ := fetchUrlFromNasaApi(endDateStr)
	if firstStoreUrl != fistNasaApiUrl || lastStoreUrl != lastNasaApiUrl {
		t.Error("urls retrieved from store and nasa api are different")
	}
}
