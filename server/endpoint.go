package server

import (
	"encoding/json"
	"fmt"
	"github.com/sirupsen/logrus"
	"net/http"
	"url-collector/store"
)

func getUrls(w http.ResponseWriter, r *http.Request) {
	startDateStr := r.URL.Query().Get("start_date")
	endDateStr := r.URL.Query().Get("end_date")

	s := store.GetStore()
	urls, err := s.GetUrls(startDateStr, endDateStr)
	if err != nil {
		respondWithError(w, err)
		return
	}

	urlsBytes, err := json.Marshal(urls)
	if err != nil {
		respondWithError(w, err)
		return
	}

	respondWithSuccess(w, urlsBytes)
}

func respondWithSuccess(w http.ResponseWriter, urlsBytes []byte) {
	w.WriteHeader(http.StatusOK)
	w.Header().Set("Content-Type", "application/json")
	_, err := w.Write([]byte(fmt.Sprintf(`{"urls": %s}`, urlsBytes)))
	if err != nil {
		logrus.Errorf("response Write err: %v", err)
	}
}

func respondWithError(w http.ResponseWriter, err error) {
	logrus.Errorf("respondWithError err: %v", err)
	w.WriteHeader(http.StatusBadRequest)
	w.Header().Set("Content-Type", "application/json")
	_, err = w.Write([]byte(fmt.Sprintf(`{"error": %s}`, err)))
	if err != nil {
		logrus.Errorf("response Write err: %v", err)
	}
}
