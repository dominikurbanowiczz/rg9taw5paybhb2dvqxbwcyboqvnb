package server

import (
	"github.com/sirupsen/logrus"
	"net/http"
	"url-collector/common"
)

func RunServer() {
	mux := http.NewServeMux()
	mux.HandleFunc("/pictures", getUrls)
	port := common.GetEnvString("PORT", "8080")
	logrus.Fatal(http.ListenAndServe(":"+port, mux))
}
